device=$1

(
echo o #
echo n #
echo p #
echo 1 #
echo   #
echo +100M  #
echo t #
echo c #
echo y

echo n #
echo p #
echo 3 #
echo   #
echo +2G  #
echo y

echo n #
echo p #
echo 2 #
echo   #
echo   #
echo y #
echo w #
) | fdisk "$device"
