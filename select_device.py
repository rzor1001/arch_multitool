import os,sys
def output(command):
    os.system('echo %s > select_device.out' % str(command))

disks_list = os.popen('lsblk -r').read()
disks_list = disks_list.split('\n')
disks = []
for disk in disks_list:
    disk = disk.split(' ')
    if len(disk) > 5 and disk[5]=='disk': disks.append(disk[0])
if len(disks) == 0: output('error')
ok = True
try_count = 0
while ok:
    try_count+=1
    print ''
    for count in range(len(disks)): print str(count + 1) + ') ' + disks[count]
    selected_disk = raw_input('Select disk: ')
    try:
	if int(selected_disk) != 0:
		output('/dev/' + disks[int(selected_disk)-1])
		ok=False
    except Exception as e:
	if try_count >= 3:
		output('error')
		ok=False

