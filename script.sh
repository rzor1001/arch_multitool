#!/bin/bash
device=""
arch_link="http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-2-latest.tar.gz"
arch_archive="ArchLinuxARM-rpi-2-latest.tar.gz"
echo "[downloading required packages]"
if [[ "$(which pacman)" != "" ]] &>/dev/null; then
	timeout 30 pacman-key --init &> /dev/null
	timeout 30 pacman-key --populate archlinuxarm &> /dev/null
	pacman -Sy &> /dev/null
	pacman -S --needed --noconfirm python2 python dosfstools htop &> /dev/null
fi

if [[ "$(which apt-get)" != "" ]] &>/dev/null; then
	apt-get install --yes python bsdtar dosfstools &> /dev/null
fi

echo $'\n1)Create ArchLinux SD Card (need 400MB free space)\n2)Configure system\n3)Set static IP address'
read -p "Select: " start_selection

if [[ "$start_selection" == "1" ]]; then
	echo $'\n1)Download Image\n2)Local Image'
	read -p "Select: " image_selection
	if [[ "$image_selection" == "1" ]]; then
		echo "[downloading Arch Linux archive]"
		wget $arch_link &>/dev/null
	elif [[ "$image_selection" == "2" ]]; then
		read -p "Enter image name(absolute path): " arch_archive
		if [ ! -f $arch_archive ]; then
			echo "File not found!"
			exit 1
		fi
	else
		echo "Invalid number"
		exit 1
	fi
	python2 select_device.py
	while IFS='' read -r line || [[ -n "$line" ]]; do
		if [[ "$line" == "error" ]]; then
			exit 1
		else
			echo "[creating partitions]"
			umount "$line*" &>/dev/null
			./part_sd.sh "$line" &>/dev/null
			device=$line
		fi
	done < "select_device.out"
	if  [[ $device == /dev/mmcblk* ]]; then
		one="p1"
		two="p2"
		three="p3"
	else
		one="1"
		two="2"
		three="3"
	fi
	echo "[formatting the sd card]"
	yes | mkfs.vfat "$device$one" &>/dev/null
	yes | mkfs.ext4 "$device$two" &>/dev/null
	yes | mkfs.ext4 "$device$three" &>/dev/null
	echo "[creating folders for mounting]"
	mkdir root &>/dev/null
	mkdir boot &>/dev/null
	mkdir home &>/dev/null
	echo "[mounting SD card partitons]"
	mount "$device$one" boot &>/dev/null
	mount "$device$two" root &>/dev/null
	mount "$device$three" home &>/dev/null
	echo "[unpacking Arch Linux archive]"
	bsdtar -xpf "$arch_archive" -C root &>/dev/null
	sync
	echo "[move boot directory]"
	cp -r root/boot/* boot/ &>/dev/null
	echo "[move home directory]"
	mkdir home/home
	cp -r -p root/home/* home/home/ &>/dev/null
	echo "[changing fstab file]"
	echo "/dev/mmcblk0p3	/mnt/data	ext4	noatime,relatime	0	0" >> root/etc/fstab
	echo "/mnt/data/home	/home	none	bind	0	0" >> root/etc/fstab
	echo "/mnt/data/media	/media	none	bind	0	0" >> root/etc/fstab
	echo "tmpfs	/var/log	tmpfs	nodev,nosuid	0	0" >> root/etc/fstab
	echo "tmpfs	/var/tmp	tmpfs	nodev,nosuid	0	0" >> root/etc/fstab
	echo "tmpfs	/tmp	tmpfs	nodev,nosuid	0	0" >> root/etc/fstab
	mkdir root/media
	mkdir home/home/alarm/arch_multitool &>/dev/null
	git clone https://rzor1001@bitbucket.org/rzor1001/arch_multitool.git home/home/alarm/arch_multitool
	#cp -r {script.sh,select_device.py,ssh} home/home/arch_multitool/ &>/dev/null
	chmod 755 home/home/alarm/arch_multitool/*
	read -p "Set static IP address(y/n): " set_static
	if [[ "$set_static" == "y" ]] || [ "$set_static" == "Y" ]; then
		read -p "Enter device address(with mask): " ip_address
		read -p "Enter gateway address: " gateway_address
		read -p "Enter DNS1 address: " dns1_address
		read -p "Enter DNS2 address: " dns2_address
		echo ""
		echo "Are you sure?"
		echo "Device address(with mask): $ip_address"
		echo "Gateway address: $gateway_address"
		echo "DNS1 address: $dns1_address"
		echo "DNS2 address: $dns2_address"
		read -p "Type y/n: " user_response
		if [ "$user_response" == "y" ] || [ "$user_response" == "Y" ]; then
			echo "[Match]" > root/etc/systemd/network/eth0.network
			echo "Name=eth0" >> root/etc/systemd/network/eth0.network
			echo "[Network]" >> root/etc/systemd/network/eth0.network
			echo "Address=$ip_address" >> root/etc/systemd/network/eth0.network
			echo "Gateway=$gateway_address" >> root/etc/systemd/network/eth0.network
			echo "DNS=$dns1_address" >> root/etc/systemd/network/eth0.network
			echo "DNS=$dns2_address" >> root/etc/systemd/network/eth0.network
		fi
	fi
	echo "[unmounting SD card partitions]"
	umount boot root home &>/dev/null
	if [[ "$image_selection" == "1" ]]; then
		rm $arch_archive &>/dev/null
	fi

elif [[ "$start_selection" == "2" ]]; then
	echo "[Upgrading system]"
	pacman -Syu --noconfirm &> /dev/null
	echo "[adding pi user]"
	useradd -m pi &>/dev/null
	userdel alarm &>/dev/null
	echo "[creating password for pi: quovadis.18]"
	(
	echo quovadis.18
	echo quovadis.18
	) | passwd pi &> /dev/null
	echo "[copy ssh key]"
	cp -r ssh/* /etc/ssh/ &> /dev/null
	echo "[installing sudo]"
	pacman -S --needed --noconfirm sudo &> /dev/null
	echo "[adding pi to sudoers file]"
	echo "pi ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

elif [[ "$start_selection" == "3" ]]; then
	read -p "Enter device address(with mask): " ip_address
	read -p "Enter gateway address: " gateway_address
	read -p "Enter DNS1 address: " dns1_address
	read -p "Enter DNS2 address: " dns2_address
	echo ""
	echo "Are you sure?"
	echo "Device address(with mask): $ip_address"
	echo "Gateway address: $gateway_address"
	echo "DNS1 address: $dns1_address"
	echo "DNS2 address: $dns2_address"
	read -p "Type y/n: " user_response
	if [ "$user_response" == "y" ] || [ "$user_response" == "Y" ]; then
		echo "[Match]" > /etc/systemd/network/eth0.network
		echo "Name=eth0" >> /etc/systemd/network/eth0.network
		echo "[Network]" >> /etc/systemd/network/eth0.network
		echo "Address=$ip_address" >> /etc/systemd/network/eth0.network
		echo "Gateway=$gateway_address" >> /etc/systemd/network/eth0.network
		echo "DNS=$dns1_address" >> /etc/systemd/network/eth0.network
		echo "DNS=$dns2_address" >> /etc/systemd/network/eth0.network
	else
		exit 1
	fi
else
    echo "Invalid number"
fi
